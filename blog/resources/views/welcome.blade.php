<!DOCTYPE html>
<!-- Author ="I Made Tangkas Wahyu Kencana Yuda" -->
<html lang="en">

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Selamat Datang di SanberBook</title>
    </head>
    <body>
        <!-- Move Page -->
        <div>
            <a href="/">Home</a>
        </div>
        <h1>SELAMAT DATANG {{$nama_Depan}} {{$nama_Belakang}}!</h1>
        <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
    </body>
    <p>&copy; SanberBook</p>
</html>
