@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
	<div class="card">
		<div class="mt-3 ml-3">
			<h3> {{$pertanyaan->judul}} </h3>
			<p> {{$pertanyaan->isi}} </p>
		</div>
	</div>
</div>
@endsection
