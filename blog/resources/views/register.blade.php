<!DOCTYPE html>
<!-- Author ="I Made Tangkas Wahyu Kencana Yuda" -->
<html lang="en">

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Sign Up Sanberbook Hari 1 – Berlatih HTML</title>
    </head>

    <body>
        <!-- Move Page -->
        <div>
            <a href="/">Home</a>
        </div>

        <div>
            <h1>Buat Account <span>Baru!</span></h1>
            <h2>Sign Up Form</h2>
        </div>

        <form method="POST" action="/welcome">
            @csrf
            <!-- First Name -->
            <label for="First_Name">First Name:</label>
            <br>
            <br>
            <input type="text" name="First Name" id="First_Name">
            <br>
            <br>

            <!-- Last Name -->
            <label for="Last_Name">Last Name:</label>
            <br> <br>
            <input href="text" name="Last Name" id="Last_Name">
            <br> <br>

            <!-- Gender -->
            <label>Gender:</label> <br> <br>
            <input type="radio" name="Gender" value="Man"> Man
            <br>
            <input type="radio" name="Gender" value="Woman"> Woman
            <br>
            <input type="radio" name="Gender" value="Other"> Other
            <br> <br>

            <!-- Nationality -->
            <label>Nationality</label> <br> <br>
            <select>
                <option value="Indonesia">Indonesia</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select>
            <br> <br>

            <!-- Language Spoken -->
            <label>Language Spoken:</label> <br> <br>
            <input type="checkbox" name="Language_Spoken" value="1"> Bahasa Indonesia
            <br>
            <input type="checkbox" name="Language_Spoken" value="2"> English
            <br>
            <input type="checkbox" name="Language_Spoken" value="3"> Arabic
            <br>
            <input type="checkbox" name="Language_Spoken" value="3"> Japanese
            <br> <br>

            <!-- Bio -->
            <label for="Bio">Bio:</label>
            <br> <br>
            <textarea cols="32" rows="10" id="Bio"></textarea>
            <br>

            <!-- Sign Up -->
            <button type="submit">Sign Up</button>
        </form>
        <p>&copy; SanberBook</p>
    </body>
</html>
