<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    // untuk menampilkan form register
    public function index()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $nama_Depan = strtoupper($request->First_Name);
        $nama_Belakang = strtoupper($request->Last_Name);
        return view('welcome', compact('nama_Depan', 'nama_Belakang'));
    }
}
